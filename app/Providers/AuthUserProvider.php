<?php
/**
 * @author Mahabubul Hasan <codehasan@gmail.com>
 * Date: 12/1/2017
 * Time: 12:18 PM
 */

namespace App\Providers;

use Illuminate\Auth\GenericUser;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\UserProvider;
use Parse\ParseException;
use Parse\ParseUser;

class AuthUserProvider implements UserProvider
{
    public function retrieveById($identifier)
    {
        $query = ParseUser::query();
        $parse = $query->get($identifier);
        return $this->getGenericUser($this->wrapUser($parse));
    }

    public function retrieveByToken($identifier, $token)
    {
        dd('retrivedByToken');
    }

    public function updateRememberToken(Authenticatable $user, $token)
    {
        // TODO: Implement updateRememberToken() method.
    }

    public function retrieveByCredentials(array $credentials)
    {
        try {
            $parse = ParseUser::logIn($credentials['username'], $credentials['password']);
            $user = $this->wrapUser($parse);

        } catch (ParseException $error) {
            return null;
        }

        return $this->getGenericUser($user);
    }

    public function validateCredentials(Authenticatable $user, array $credentials)
    {
        return ($user)?true:false;
    }

    protected function wrapUser($parse){
        $user['id'] = $parse->getObjectId();
        $data['firstName'] = $parse->get('firstName');
        $data['lastName'] = $parse->get('lastName');
        $data['name'] = $parse->get('name');
        $data['username'] = $parse->get('username');
        $data['email'] = $parse->get('email');
        $data['vehicles'] = $parse->get('vehicles');
        $data['occupation'] = $parse->get('occupation');
        return $data;
    }

    /**
     * Get the generic user.
     *
     * @param  mixed  $user
     * @return \Illuminate\Auth\GenericUser|null
     */
    protected function getGenericUser($user)
    {
        if (! is_null($user)) {
            return new GenericUser((array) $user);
        }
    }


}