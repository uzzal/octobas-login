<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Auth;
use Parse\ParseClient;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        ParseClient::initialize( '5a0b7ce3af8b5c4592cb23a4', null, '7c048aeb-186f-4166-a50e-193b53f68d44' );
        ParseClient::setServerURL('https://dashboard.octobas.com/5a0b7ce3af8b5c4592cb23a4','parse');

        Auth::provider('octobas', function($app, array $config){
            return new AuthUserProvider();
        });
    }
}
